package pl.norb.iot_comfort.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class HumidityModel implements Parcelable {

    private int value;
    private Date date;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.value);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
    }

    public HumidityModel() {
    }

    protected HumidityModel(Parcel in) {
        this.value = in.readInt();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
    }

    public static final Creator<HumidityModel> CREATOR = new Creator<HumidityModel>() {
        @Override
        public HumidityModel createFromParcel(Parcel source) {
            return new HumidityModel(source);
        }

        @Override
        public HumidityModel[] newArray(int size) {
            return new HumidityModel[size];
        }
    };
}
