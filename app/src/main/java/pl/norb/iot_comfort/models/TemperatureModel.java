package pl.norb.iot_comfort.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class TemperatureModel implements Parcelable {

    private Float temperature;
    private Date date;

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.temperature);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
    }

    public TemperatureModel() {
    }

    protected TemperatureModel(Parcel in) {
        this.temperature = (Float) in.readValue(Float.class.getClassLoader());
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
    }

    public static final Creator<TemperatureModel> CREATOR = new Creator<TemperatureModel>() {
        @Override
        public TemperatureModel createFromParcel(Parcel source) {
            return new TemperatureModel(source);
        }

        @Override
        public TemperatureModel[] newArray(int size) {
            return new TemperatureModel[size];
        }
    };
}
