package pl.norb.iot_comfort;

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import pl.norb.iot_comfort.models.HumidityModel;
import pl.norb.iot_comfort.models.TemperatureModel;

public class ChartActivity extends AppCompatActivity {

    private static String TEMEPERATURE_MODEL_INTENT = "temperature_model_intent";
    private static String HUMIDITY_MODEL_INTENT = "humidity_model_intent";

    @BindView(R.id.lineChart)
    LineChart lineChart;


    private List<TemperatureModel> temperatureModelList;
    private List<HumidityModel> humidityModelList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        ButterKnife.bind(this);
        onActivityReady();
    }

    private void onActivityReady() {
        temperatureModelList = getIntent().getParcelableArrayListExtra(TEMEPERATURE_MODEL_INTENT);
        humidityModelList = getIntent().getParcelableArrayListExtra(HUMIDITY_MODEL_INTENT);
        initChart();
    }

    private void initChart() {
        lineChart.getDescription().setEnabled(true);
        lineChart.setTouchEnabled(true);

        List<Entry> temperatureValues = new ArrayList<>();
        for (int i = 0; i < temperatureModelList.size(); i++) {
            temperatureValues.add(new Entry(i, temperatureModelList.get(i).getTemperature()));
        }

        LineDataSet temperatureSet = new LineDataSet(temperatureValues, getString(R.string.temperature_chart_description));
        temperatureSet.setColor(Color.RED);
        temperatureSet.setCircleColor(Color.RED);

        List<Entry> humidityValues = new ArrayList<>();
        for (int i = 0; i < humidityModelList.size(); i++) {
            humidityValues.add(new Entry(i, humidityModelList.get(i).getValue()));
        }

        LineDataSet humiditySet = new LineDataSet(humidityValues, getString(R.string.humidity_chart_description));
        humiditySet.setColor(Color.BLUE);
        humiditySet.setCircleColor(Color.BLUE);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(temperatureSet);
        dataSets.add(humiditySet);

        LineData data = new LineData(dataSets);

        lineChart.setData(data);
    }
}
