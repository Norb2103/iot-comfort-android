package pl.norb.iot_comfort;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import pl.norb.iot_comfort.backend.ApiClient;
import pl.norb.iot_comfort.backend.ApiService;
import pl.norb.iot_comfort.models.HumidityModel;
import pl.norb.iot_comfort.models.TemperatureModel;

public class MainActivity extends AppCompatActivity {

    private static String SHARED_PREFERENCES_NAME = "Shared_Preferences";
    private static String PREFS_TEMPERATURE_NAME = "Temperature_value";
    private static String PREFS_HUMIDITY_NAME = "Humidity_value";
    private static String TEMEPERATURE_MODEL_INTENT = "temperature_model_intent";
    private static String HUMIDITY_MODEL_INTENT = "humidity_model_intent";

    private ApiService apiService;
    private List<TemperatureModel> temperatureModelList = new ArrayList<>();
    private List<HumidityModel> humidityModelList = new ArrayList<>();
    private int comfortColor;
    private SharedPreferences.Editor editor;
    private SharedPreferences prefs;

    @BindView(R.id.temperature)
    TextView temperatureText;
    @BindView(R.id.humidity)
    TextView humidityText;
    @BindView(R.id.comfort_background)
    RelativeLayout comfortBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        onActivityReady();
    }

    @OnClick(R.id.refresh)
    public void onRefreshClick() {
        updateTemperature();
        updateHumidy();
    }

    @OnClick(R.id.chart)
    public void onChartClick() {
        if (!temperatureModelList.isEmpty() && !humidityModelList.isEmpty()) {
            Intent intent = new Intent(this, ChartActivity.class);
            intent.putParcelableArrayListExtra(TEMEPERATURE_MODEL_INTENT, (ArrayList<? extends Parcelable>) temperatureModelList);
            intent.putParcelableArrayListExtra(HUMIDITY_MODEL_INTENT, (ArrayList<? extends Parcelable>) humidityModelList);
            startActivity(intent);
        } else {
            //todo show intent with no data
        }
    }

    @OnClick(R.id.comfort)
    public void onComfortClick() {
        updateComfort();
    }

    private void onActivityReady() {
        getApiService();
        comfortColor = R.color.default_color;
        editor = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE).edit();
        prefs = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        getCacheValues();
        updateTemperature();
        updateHumidy();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

    }

    private void getApiService() {
        ApiClient apiClient = new ApiClient();
        apiService = apiClient.getClient().create(ApiService.class);
    }

    private void updateTemperature() {
        apiService.getTemperature()
                .subscribe(new SingleObserver<List<TemperatureModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<TemperatureModel> temperatureModels) {
                        temperatureModelList = temperatureModels;
                        DecimalFormat df = new DecimalFormat();
                        df.setMaximumFractionDigits(1);
                        setTemperature(df.format(temperatureModels.get(temperatureModels.size() - 1).getTemperature()));
                        saveTemperatureInCache(df.format(temperatureModels.get(temperatureModels.size() - 1).getTemperature()));
                    }

                    @Override
                    public void onError(Throwable e) {
                        //todo show error mesage
                        Log.e("TAG", "err");
                    }
                });
    }

    private void updateHumidy() {
        apiService.getHumidity()
                .subscribe(new SingleObserver<List<HumidityModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<HumidityModel> humidityModels) {
                        humidityModelList = humidityModels;
                        setHumidity(humidityModels.get(humidityModels.size() - 1).getValue());
                        saveHumidityInCache(humidityModels.get(humidityModels.size() - 1).getValue());
                    }

                    @Override
                    public void onError(Throwable e) {
                        //todo show error mesage
                        Log.e("TAG", "err");
                    }
                });
    }

    private void updateComfort() {
        apiService.getComfort()
                .subscribe(new SingleObserver<Float>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Float value) {
                        setComfort(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        //todo show error mesage
                        Log.e("TAG", "err");
                    }
                });
    }

    private void setTemperature(String temperature) {
        temperatureText.setText(temperature);
    }

    private void setHumidity(int humidity) {
        humidityText.setText(String.format(getString(R.string.humidity_text), humidity));
    }

    private void setComfort(float comfort) {
        if (comfort < -2.5) {
            changeColorWithAnimation(R.color.the_coldest_color);
            comfortColor = R.color.the_coldest_color;
        } else if (comfort >= - 2.5 && comfort < - 1.5) {
            changeColorWithAnimation(getColor(R.color.colder_color));
            comfortColor = R.color.colder_color;
        } else if (comfort >= -1.5 && comfort < - 0.5) {
            changeColorWithAnimation(getColor(R.color.cold_color));
            comfortColor = R.color.cold_color;
        } else if (comfort >= - 0.5 && comfort < 0.5) {
            changeColorWithAnimation(getColor(R.color.comfort_color));
            comfortColor = R.color.comfort_color;
        } else if (comfort >= 0.5 && comfort < 1.5) {
            changeColorWithAnimation(getColor(R.color.hot_color));
            comfortColor = R.color.hot_color;
        } else if (comfort >= 1.5 &&  comfort < 2.5) {
            changeColorWithAnimation(getColor(R.color.hotter_color));
            comfortColor = R.color.hotter_color;
        } else if (comfort >= 2.5) {
            changeColorWithAnimation(getColor(R.color.the_hottest_color));
            comfortColor = R.color.the_hottest_color;
        } else {
                changeColorWithAnimation(getColor(R.color.default_color));
                comfortColor = R.color.default_color;
        }
    }

    private void changeColorWithAnimation(int color) {
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), comfortColor, color);
        colorAnimation.setDuration(500);
        colorAnimation.addUpdateListener(animator -> comfortBackground.setBackgroundColor((int) animator.getAnimatedValue()));
        colorAnimation.start();
    }

    private void saveTemperatureInCache(String temp) {
        editor.putString(PREFS_TEMPERATURE_NAME, temp);
        editor.apply();
    }

    private void saveHumidityInCache(int humidity) {
        editor.putInt(PREFS_HUMIDITY_NAME, humidity);
        editor.apply();
    }

    private void getCacheValues() {
        setTemperature(prefs.getString(PREFS_TEMPERATURE_NAME, "20"));
        setHumidity(prefs.getInt(PREFS_HUMIDITY_NAME, 50));
    }
}
