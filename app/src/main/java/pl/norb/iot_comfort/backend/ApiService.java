package pl.norb.iot_comfort.backend;

import java.util.List;

import pl.norb.iot_comfort.models.HumidityModel;
import pl.norb.iot_comfort.models.TemperatureModel;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface ApiService {

    @GET("/api/temperature")
    Single<List<TemperatureModel>> getTemperature();

    @GET("/api/humidity")
    Single<List<HumidityModel>> getHumidity();

    @GET("/api/comfort")
    Single<Float> getComfort();


}
